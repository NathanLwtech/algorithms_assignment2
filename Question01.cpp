/*
Nathanael Connors
Algorithms and Data Structures
Assignment 2 - Problem 1
Stack using Linked List
*/

#include <iostream>
using namespace std;

class node
{
public:
	node *next;
	int value;
};

class stack
{
private:
	node *head;

public:
	/* Default Constructor */
	stack()
	{
		head = NULL;
	}

	/* Returns true if list is empty */
	bool empty()
	{
		return head == NULL;
	}

	/* Counts size of stack */
	int size()
	{
		int counter = 0;
		node *temp = head;
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			while (temp != NULL)
			{
				counter++;
				temp = temp->next;
			}
			return counter;
		}
		
	}

	/* Same as peek function in class */
	int top()
	{
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			return head->value;
		}
	}

	/* Pushes new item onto stack */
	void push(int n)
	{
		node *newNode = new node();
		newNode->value = n;
		newNode->next = head;
		head = newNode;
	}

	/* Removes and deletes from stack */
	void pop()
	{
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			node *temp = head;
			head = head->next;
			delete temp;
		}
	}
};

int main()
{
	/* Testing the stack class */
	stack myStack;

	// returns empty as true
	myStack.empty() ? cout << "Empty \n" : cout << "Not Empty \n";

	// push elements onto stack
	myStack.push(4);
	myStack.push(1);
	myStack.push(7);
	myStack.push(6);

	// return empty as false
	myStack.empty() ? cout << "Empty \n" : cout << "Not Empty \n";

	// pop element off of stack
	myStack.pop();

	// checks top of stack
	cout << myStack.top() << endl;

	// count stack elements
	cout << myStack.size() << endl;

	cin.get();
	return 0;
}