/*
Nathanael Connors
Algorithms and Data Structures
Assignment 2 - Problem 2
Queue using Linked List
*/

#include <iostream>
using namespace std;

class node
{
public:
	node *next;
	int value;
};

class queue
{
private:
	node *back;
	node *front;

public:
	/* Default Constructor */
	queue()
	{
		back = NULL;
		front = NULL;
	}

	/* Returns true if Queue is empty */
	bool empty()
	{
		return front == NULL;
	}

	/* Travels Queue to count elements */
	int size()
	{
		int counter = 0;
		node *cNode = new node;
		cNode = front;
		
		if (front == NULL)
		{
			throw new exception("Queue is empty");
		}
		else {
			while (cNode != NULL)
			{
				counter++;
				cNode = cNode->next;
			}
			return counter;
		}
	}

	/* Removes element from Queue */
	int dequeue()
	{
		node *temp = new node;
		
		if (front == NULL)
		{
			throw new exception("Queue is empty");
		}
		else
		{
			temp = front;
			front = front->next;
			delete temp;
		}

	}

	/* Adds element to Queue */
	void enqueue(int n)
	{
		node *newNode = new node;
		newNode->value = n;
		newNode->next = NULL;
		
		front == NULL ? front = newNode : back->next = newNode;

		back = newNode;
	}
};

int main()
{
	/* Testing Queue Class */
	queue myQueue;

	// returns empty as true
	myQueue.empty() ? cout << "Empty \n" : cout << "Not Empty \n";

	// add elements to queue
	myQueue.enqueue(4);
	myQueue.enqueue(6);
	myQueue.enqueue(9);
	myQueue.enqueue(7);

	// remove element from queue
	myQueue.dequeue();

	// returns empty as false
	myQueue.empty() ? cout << "Empty \n" : cout << "Not Empty \n";

	// count queue elements
	cout << myQueue.size() << endl;

	cin.get();
	return 0;
}