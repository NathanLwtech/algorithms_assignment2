/*
Nathanael Connors
Algorithms and Data Structures
Assignment 2 - Problem 3
Find Missing Number in Stack
*/

#include <iostream>
using namespace std;

class node
{
public:
	node *next;
	int value;
};

class stack
{
public:
	node *head;
	/* Default Constructor */
	stack()
	{
		head = NULL;
	}

	/* Returns true if list is empty */
	bool empty()
	{
		return head == NULL;
	}

	/* Counts size of stack */
	int size()
	{
		int counter = 0;
		node *temp = head;
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			while (temp != NULL)
			{
				counter++;
				temp = temp->next;
			}
			return counter;
		}

	}

	/* Same as peek function in class */
	int top()
	{
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			return head->value;
		}
	}

	/* Pushes new item onto stack */
	void push(int n)
	{
		node *newNode = new node();
		newNode->value = n;
		newNode->next = head;
		head = newNode;
	}

	/* Removes and deletes from stack */
	void pop()
	{
		if (head == NULL)
		{
			throw new exception("Stack is empty");
		}
		else
		{
			node *temp = head;
			head = head->next;
			delete temp;
		}
	}
};

int Missing(stack s);

int main()
{	
	stack testcase1, testcase2, testcase3, testcase4, testcase5, testcase6;

	// {} -> 1
	cout << "Test Case 1: " << Missing(testcase1) << endl;

	// {1} -> 2
	testcase2.push(1);
	cout << "Test Case 2: " << Missing(testcase2) << endl;

	// {2} -> 1
	testcase3.push(2);
	cout << "Test Case 3: " << Missing(testcase3) << endl;

	// {1, 3} -> 2
	testcase4.push(1);
	testcase4.push(3);
	cout << "Test Case 4: " << Missing(testcase4) << endl;

	// {1, 2} -> 3
	testcase5.push(1);
	testcase5.push(2);
	cout << "Test Case 5: " << Missing(testcase5) << endl;

	// {1, 4, 3, 7, 6, 2} -> 5
	testcase6.push(1);
	testcase6.push(4);
	testcase6.push(3);
	testcase6.push(7);
	testcase6.push(6);
	testcase6.push(2);
	cout << "Test Case 6: " << Missing(testcase6) << endl;

	cin.get();
	return 0;
}

int Missing(stack s)
{
	node *cNode = s.head;
	if (s.head == NULL)
	{
		return 1;
	}
	else
	{
		int sum = 0;
		for (cNode = s.head; cNode != NULL; cNode = cNode->next)
		{
			sum = sum + cNode->value;
		}
		int n = s.size() + 1;
		int missingNum = (n * (n + 1) / 2) - sum;

		return missingNum;
	}
}